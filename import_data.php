<?php

require_once("wp-load.php");
// From URL to get webpage contents. 
//$url = "https://api.apify.com/v2/datasets/sFhF66gXQjhWhhc4R/items"; 

$url= "https://api.apify.com/v2/actor-tasks/BeFZiv66vsAEzzhxx/runs/last/dataset/items?token=yw7gopCjpZbSZBhysPrdb3EqR";
  
// Initialize a CURL session. 
$ch = curl_init();  
  
// Return Page contents. 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
  
//grab URL and pass it to the variable. 
curl_setopt($ch, CURLOPT_URL, $url); 
  
$result = curl_exec($ch); 
  
if(!empty($result)){
  $json= json_decode($result,true);
  /*echo '<pre/>';
  print_r($json);*/
  for($i=0;$i<count($json);$i++){

    $map_zoom="8";
    $price = trim($json[$i]["price"]);
    $price_avg ="0";
    if(!empty($price)){
      preg_match_all('!\d+!', $price, $matches);
      if(!empty($matches)){
        $price_avg = @$matches[0][0];
      }
    } 
    $hotel_star= trim($json[$i]["rating"]);
  	$title = $json[$i]["title"];
    $post_slug = sanitize_title_with_dashes ($title,'','save');
    $post_slugsan = sanitize_title($post_slug); 
  	$data["post_title"] = $json[$i]["title"];
  	$data["post_content"] = $json[$i]["description"];
  	$data["post_date"] = date("Y-m-d H:i:s");
  	$data["post_date_gmt"] = date("Y-m-d H:i:s");
  	$data["post_status"] = "publish";
  	$data["post_type"] = "st_hotel";
  	$data["post_author"]="2";
  	$data["post_name"]=$post_slugsan;
  	global $wpdb;
  	$wpdb->insert("wp_posts",$data);
    $last_id = $wpdb->insert_id;

    $latlong=@explode(",",$json[$i]["latlong"]);

    $hotel_data["post_id"]=$last_id;
    $hotel_data["multi_location"] ="_8281_";
    $hotel_data["id_location"]="";
    $hotel_data["address"]=$json[$i]["address"];
    $hotel_data["allow_full_day"]="on";
    $hotel_data["rate_review"]="0";
    $hotel_data["hotel_star"]=$hotel_star;
    $hotel_data["price_avg"]=$price_avg;
    $hotel_data["min_price"]=$price_avg;
    $hotel_data["hotel_booking_period"]="0";
    $hotel_data["map_lat"]=@$latlong[0];
    $hotel_data["map_lng"]=@$latlong[1];
    $hotel_data["is_sale_schedule"]="";
    $hotel_data["post_origin"]="";
    $hotel_data["is_featured"]="";
    $wpdb->insert("wp_st_hotel",$hotel_data);

    $relation_data["post_id"]=$last_id;
    $relation_data["location_from"]="8281";
    $relation_data["location_to"]="0";
    $relation_data["post_type"]="st_hotel";
    $relation_data["location_type"]="multi_location";
    $wpdb->insert("wp_st_location_relationships",$relation_data);




  	$st_google_map["lat"] = @$latlong[0];
  	$st_google_map["lng"] = @$latlong[1];
  	$st_google_map["zoom"] = $map_zoom;
  	$st_google_map["type"] = "";
  	$map = serialize($st_google_map);
  	$wpdb->insert("wp_postmeta",array("meta_key"=>'st_google_map',"meta_value"=>$map,"post_id"=>$last_id));

     update_post_meta($last_id,"booking_com_url",$json[$i]["url"]);


     update_post_meta($last_id,"hotel_policy_booking_com",$json[$i]["hotel_policy"]);
     

     update_post_meta($last_id,"rate_review","0");
     update_post_meta($last_id,"map_lat",@$latlong[0]);
     update_post_meta($last_id,"map_lng",@$latlong[1]);
     update_post_meta($last_id,"map_zoom",$map_zoom);
     update_post_meta($last_id,"map_type","");
     update_post_meta($last_id,"price_avg",$price_avg);
     update_post_meta($last_id,"min_price",$price_avg);
     update_post_meta($last_id,"id_location","");
     update_post_meta($last_id,"min_book_room","0");
     update_post_meta($last_id,"hotel_booking_period","0");
     update_post_meta($last_id,"is_featured","off");
     
     update_post_meta($last_id,"multi_location","_8281_");
     update_post_meta($last_id,"address",$json[$i]["address"]);
     update_post_meta($last_id,"enable_street_views_google_map","on");
     update_post_meta($last_id,"hotel_star",$hotel_star);
     update_post_meta($last_id,"is_auto_caculate","on");
     update_post_meta($last_id,"allow_full_day","on");

     
     
     
    $hotel_image = $json[$i]["hotel_image"];
  	$gallery = array();
    if(!empty($hotel_image)){
    	
	    for($j=0;$j< count($hotel_image);$j++){

          

          $image_url = $hotel_image[$j]["src"];
          if($image_url != "#"){
              $exp=explode("/",$image_url);
              $image_url="https://q-cf.bstatic.com/images/hotel/max1024x768/".$exp[6]."/".$exp[7];


        		 $wordpress_upload_dir = wp_upload_dir();
        		 $url = $image_url;
        		 $name = pathinfo($url,PATHINFO_EXTENSION);
        		 $new_file_mime ="image/".$name;
        		 $profilepicture['name'] = basename($url);
        		 $new_file_path = $wordpress_upload_dir['path'] . '/' . $profilepicture['name'];
        		 $status= @file_put_contents($new_file_path, file_get_contents($url));
        		  if(!empty($status)){
        			  $upload_id = wp_insert_attachment( array(
        			    'guid'           => $new_file_path, 
        			    'post_mime_type' => $new_file_mime,
        			    'post_title'     => preg_replace( '/\.[^.]+$/', '', $profilepicture['name'] ),
        			    'post_content'   => '',
        			    'post_status'    => 'inherit'
        			  ), $new_file_path );

    	           array_push($gallery,$upload_id);
    	           if($j == 0){
              	     setFeaturedImage($last_id,$image_url);
                   }
               }
          }
	    }
    }

  	update_post_meta($last_id,"gallery",implode(",",$gallery));

    $popular_facilities = $json[$i]["popular_facilities"];
    if(!empty($popular_facilities)){
      //setHotelFacilities($last_id,$popular_facilities);
      update_post_meta($last_id,"popular_facilities_booking_com",$json[$i]["popular_facilities"]);
    }
    update_post_meta($last_id,"common_facilities_booking_com",$json[$i]["common_facilities"]);
  }	
}

function setHotelFacilities($post_id,$popular_facilities){
    $taxonomy = "hotel_facilities";
    if(!empty($popular_facilities)){
      for($p=0;$p<count($popular_facilities);$p++){
         $facilities = trim($popular_facilities[$p]["src"]);
         $cat  = get_term_by('name', $facilities , $taxonomy);
         if($cat == false){
           $cat = wp_insert_term($facilities,$taxonomy);
           $cat_id = $cat['term_id'] ;
         }else{
           $cat_id = $cat->term_id ;
         }
         $res=wp_set_post_terms($post_id,array($cat_id),$taxonomy ,true);
      }
    }
} 

function setFeaturedImage($post_id,$image_url){
	$wordpress_upload_dir = wp_upload_dir();
	$url = $image_url;
	$name = pathinfo($url,PATHINFO_EXTENSION);
	$new_file_mime ="image/".$name;
	$picture['name'] = basename($url);
	$new_file_path = $wordpress_upload_dir['path'] . '/' . $picture['name'];
	$status= @file_put_contents($new_file_path, file_get_contents($url));
	if(!empty($status)){
	    $attachment = array(
	        'post_mime_type' => $new_file_mime,
	        'post_title' => sanitize_file_name($picture['name']),
	        'post_content' => '',
	        'post_status' => 'inherit'
	    );
	    $attach_id = wp_insert_attachment( $attachment, $new_file_path, $post_id );
	    require_once(ABSPATH . 'wp-admin/includes/image.php');
	    $attach_data = wp_generate_attachment_metadata( $attach_id, $new_file_path );
	    wp_update_attachment_metadata( $attach_id, $attach_data );
	    set_post_thumbnail( $post_id, $attach_id );		 	
	}    
} 
?>