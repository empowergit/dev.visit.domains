
<div class="mailchimp">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-10 col-lg-offset-2">
                <div class="row">
                    <div class="col-xs-12  col-md-12 col-lg-12">
                        <div class="media ">
                            <div class="media-left pr30 hidden-xs">
                                <!-- <img class="media-object"
                                     src="<?php echo get_template_directory_uri() ?>/v2/images/svg/ico_email_subscribe.svg"
                                     alt=""> -->
                                     <img class="" src="<?php echo get_stylesheet_directory_uri() ?>/img/newsletter.png" alt="newsletter">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading st-heading-section f24 newsletter-heading"><?php echo esc_html__( 'News and Offers to Your Inbox', ST_TEXTDOMAIN ) ?></h4>
                                <p class="f16  newsletter-subheading"><?php echo esc_html__( 'Join our Mailing List for Rome Offers, Travel Tips and Events', ST_TEXTDOMAIN ) ?></p>
                                <div>
                                   <?php
                                     $form = st()->get_option( 'mailchimp_shortcode' );
                                     echo do_shortcode( $form );
                                   ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="col-xs-12 col-md-5 col-lg-6">
                        <?php
                            $form = st()->get_option( 'mailchimp_shortcode' );
                            if ( $form ) {
                                //echo do_shortcode( $form );
                            } else {
                                ?>
                                <form action="//mc.us4.list-manage.com/subscribe/form-post?u=d87b9e362c41b8821c3893aae&amp;id=67f293b078&amp;popup=true" class="subcribe-form" accept-charset="UTF-8" method="post" enctype="multipart/form-data" data-dojo-attach-point="formNode" novalidate="">
									
                                    <div class="form-group">
										<div class="content__formFields" data-dojo-attach-point="formFieldsContainer">
											<div class="field-wrapper" id="uniqName_3_0" widgetid="uniqName_3_0">
                                        <input type="email" name="EMAIL" id="mc-EMAIL" class="form-control" placeholder="<?php esc_attr_e('Your Email', ST_TEXTDOMAIN) ?>">
												<div class="invalid-error"></div>
												</div>
										
										<div style="position:absolute;left:-5000px;"><input type="text" name="b_d87b9e362c41b8821c3893aae_67f293b078" tabindex="-1" value=""></div>
											</div>
										<div class="content__button">
                                        <input type="submit" name="submit" value="<?php echo esc_attr_e('Subscribe', ST_TEXTDOMAIN) ?>" data-dojo-attach-point="submitButton">
											</div>
                                    </div>
                                </form>
                                <?php
                            }
                        ?>

                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    wp_reset_postdata();
    wp_reset_query();
    $footer_template = TravelHelper::st_get_template_footer( get_the_ID(), true );
    if ( $footer_template ) {
        $vc_content = STTemplate::get_vc_pagecontent( $footer_template );
        if ( $vc_content ) {
            echo '<footer id="main-footer" class="clearfix">';
            echo balanceTags($vc_content);
            echo ' </footer>';
        }
    } else {
        ?>
        <footer id="main-footer" class="container-fluid">
            <div class="container text-center">
                <p><?php _e( 'Copy &copy; 2014 Shinetheme. All Rights Reserved', ST_TEXTDOMAIN ) ?></p>
            </div>

        </footer>
    <?php } ?>
<div class="container main-footer-sub">
    <div class="st-flex space-between">
        <div class="left mt20">
            <div class="f14"><?php echo sprintf( esc_html__( 'Copyright © %s by', ST_TEXTDOMAIN ), date( 'Y' ) ); ?> <a
                        href="<?php echo esc_url( home_url( '/' ) ) ?>"
                        class="st-link"><?php bloginfo( 'name' ) ?></a></div>
        </div>
        <div class="right mt20">
            <img src="<?php echo get_template_directory_uri() ?>/v2/images/svg/ico_paymethod.svg" alt=""
                 class="img-responsive">
        </div>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>