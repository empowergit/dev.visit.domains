<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'visitd_wp1' );



/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );







/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'DdpUt;eQGadotXSi(DjRCHCdrFf}a:b?E~xmyJleW|NE4Lg>VVPnLU3]oMld}_(m' );
define( 'SECURE_AUTH_KEY',   'r.feWQ?>TPE01nrX^YMl$)OXnla B|^o+6-6zE(gFPg&b9ld)lk+^J}E6Xt[}?&N' );
define( 'LOGGED_IN_KEY',     'V{P?81WGt#EG>=obU5$C6gsJXup6k%&v0jE722Oj9q$7l[|c~;mYK@S[J9EW#70)' );
define( 'NONCE_KEY',         'n3a2xqAazC16YV<!s%gJ&<eP#(jvked.0k/p~q^+>T|Ipz(XmVP1coTMQV}<E$:,' );
define( 'AUTH_SALT',         '[2|dD%-9qHo2$bnG#!l]8qW0T~$@OQGRp9&FYoL.Jn%A@@&w*%keAc=FOtU-P%F&' );
define( 'SECURE_AUTH_SALT',  ':C1U%5`]f#oLh@-|C*pU{-Sl2Y9(biXBxNFAzWeZacbq:=Z*W*|Pfne<j:Qd71`-' );
define( 'LOGGED_IN_SALT',    'Q&DqML?v>(/kgoS|(EevVpnA5Xc$<usNAel}f4cG17`y}?scqd8h>1uk1CYL2??U' );
define( 'NONCE_SALT',        'bK$Rfn4?Rurl`O&iiU_#>F|<zO8BNp[f1T0g]:au2e +-ouJI* Dd.(4)og4)Y0E' );
define( 'WP_CACHE_KEY_SALT', 'M>XwLeDEJ.Lj5|asQ~KBBe7l B`R.NN?n^wvC Yy0$QvfR4#xh:YYM>Y9q5p xZ7' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
