<?php
 
// WordPress environment
require('wp-load.php');
global $wpdb; 
$sql="SELECT * FROM `wp_postmeta` WHERE `meta_key`='popular_facilities_booking_com'";
$res = $wpdb->get_results($sql,ARRAY_A);

foreach($res as $value){
  $post_id= $value["post_id"];
  $meta_value= unserialize($value["meta_value"]);
  for($i=0;$i<count($meta_value);$i++){
  	$your_string = $meta_value[$i];
  	$facilities = strip_tags($your_string);
  	setHotelFacilities($post_id,trim($facilities));
  }
}




function setHotelFacilities($post_id,$facilities){
    $taxonomy = "hotel_facilities";
         $cat  = get_term_by('name', $facilities , $taxonomy);
         if($cat == false){
           $cat = wp_insert_term($facilities,$taxonomy);
           $cat_id = $cat['term_id'] ;
         }else{
           $cat_id = $cat->term_id ;
         }
         $res=wp_set_post_terms($post_id,array($cat_id),$taxonomy ,true);
}
?>

